## MQTT IoT Project

Aim of this project is to build a system which includes a sensor and a Raspberry Pi, which also acts a MQTT client.



## Overview

Arduino has a temperature sensor and sends it to Raspberry Pi, which has a application to read inputs given to the process.

Node application reads the input and relays that to Azure IoT Hub. Maximum rate of messages should be once per minute.

Example output of Arduino is:

`
Room temperature is 22 degrees Celsius.
`

Raspberry sends HTTP request to web server, which also runs PostgreSQL database. Data is stored in database for the future.

## Prerequisite
Make sure you have
```sh
Node version 8 or newer
PostgreSQL version 9 or newer
```

#### Install Node

Go to `www.nodejs.org`and follow instructions. Download LTS or Latest version, not the one provided in `apt install`.

#### Install PostgreSQL
 
 To the terminal:
 
 `sudo apt install postgresql libpq-dev postgresql-client
postgresql-client-common -y`

Using the `pqsl` insert:

```sql
create table dripper(
time timestamp with time zone not null default now(),
id serial primary key,
temperature integer not null
);
```

If you encounter issues on Raspberry, check out:
 
`
https://opensource.com/article/17/10/set-postgres-database-your-raspberry-pi
`

## Web server

[GitLab Repository](https://gitlab.com/mahonen/dripper-server/tree/simple)

Simple web server that handles routes `GET /api/temp` and `GET /api/temp/:temperature`

Start the server locally by: `node src/server.js` and that's it!

Going to your browser you should be able to get a response from
`http://localhost:8080/api/temp`
There should be temperature readings saved into the database.

Post your own readings by going to `http://localhost:8080/api/temp/[YOUR OWN READING HERE]`

Ex: `http://localhost:8080/api/temp/26`

If you want to run it in another PC then configure the addresses accordingly.

## Arduino
Use Arduino IDE to upload the code
It should start running automatically.

## MQTT Application

[GitLab Repository](https://gitlab.com/mahonen/node-mqtt)

You can test the application in isolation with `npm run start`. This does not require a connection to Arduino. This will run `fakeInput.sh` and pipe it into the Node application.

Or you can run it in production mode with `npm run prod`, which requires you to pipe the output of Arduino app to this Node application. 

Test for working output. If connected its to the USB port. For ttyUSB0
```sh
> tail -f /dev/ttyUSB0
Room temperature is 22 degrees Celsius.
Room temperature is 22 degrees Celsius.
```
The command to run:

`
tail -f /dev/ttyUSB0 | node src/index.js
`

or modify `package.json` and change `prod` values if its not in ttyUSB0 
```javascript
 "scripts": {
    "start": "./fakeInput.sh | node src/index.js",
    "test": "echo \"Error: no test specified\" && exit 1",
    "prod": "tail -f /dev/ttyUSB0 | node src/index.js"
  },

```

## Web client
[GitLab Repository](https://gitlab.com/mahonen/dripper-client)

This is a React app which you can build to a minified bundle by using `npm run build`

How ever to test this page you can install npm package `serve` globally
by `sudo npm install -g serve`

Finally run
`serve -s build`

You should be able to connect to `http://localhost:5000` to see the results!

Replace `localhost` with your machine's IP if you need to connect from a remote machine.